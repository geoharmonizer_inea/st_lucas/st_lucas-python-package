# ST_LUCAS Python Package

Python package for accessing harmonized space-time aggregated LUCAS
(Land Use and Coverage Area frame Survey) data provided by the
[ST_LUCAS system](https://geoforall.fsv.cvut.cz/st_lucas/).

Documentation: https://geoforall.fsv.cvut.cz/st_lucas/api/

## Funding

This work is co-financed under Grant Agreement Connecting Europe
Facility (CEF) Telecom project 2018-EU-IA-0095 by the European Union.
