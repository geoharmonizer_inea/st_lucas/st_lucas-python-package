#########
Tutorials
#########

**Imports**

Python class :class:`~st_lucas.request.LucasRequest` to define a request

.. code-block:: python
                
   from st_lucas import LucasRequest

Python class :class:`~st_lucas.io.LucasIO` to input / output LUCAS points

.. code-block:: python

   from st_lucas import LucasIO

Instantiate classes to create data request

.. code-block:: python

   request = LucasRequest()

and to control data input/output

.. code-block:: python
                
   lucasio = LucasIO()

**Spatial filter**

Spatial filter by bbox property (only EPSG:3035 is supported)

.. code-block:: python

   request.bbox = (4504276, 3020369, 4689608, 3105290)

By country code

.. code-block:: python

   request.countries = ['CZ']

Example for multiple countries (Python list)

.. code-block:: python
                
   request.countries = ['CZ', 'SK']

By AOI ``aoi_polygon`` (GML format)

.. code-block:: python

   request.aoi_polygon = '''
   <Within>
       <PropertyName>lucas:geom</PropertyName>' \
       <gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="urn:ogc:def:crs:EPSG::3035" gml:id="polygon_32.geom.0">' \
           <gml:exterior>' \
               <gml:LinearRing>' \
                    <gml:posList>3014669.3195414557 4640226.080241454 2981893.140187475 4628385.701013993 2965449.7283930806 4639492.816821902 2958781.6185918115 4658392.1858341275 2977549.274784839 4672892.4477362465 3004572.819247867 4661017.510044226 3014669.3195414557 4640226.080241454 </gml:posList>' \
                </gml:LinearRing>' \
            </gml:exterior>' \
       </gml:Polygon>' \
   </Within>
   '''

**Temporal filter**

Temporal filter by single year definition

.. code-block:: python
                
   request.years = [2006]

or by multiple years (Python list)

.. code-block:: python

   request.years = [2006, 2009]

**Thematic filter**

Thematic filter by setting codes of thematic groups

* CO - Copernicus;
* FO - Forestry;
* IN - INSPIRE;
* LC_LU - Land cover, Land use;
* LC_LU_SO - Land cover, Land use, Soil]

.. code-block:: python

   request.group = 'LC_LU'

**Space-time aggregation**

Space-time aggregation by ``st_aggregated`` property we determine
whether the data should be space-time aggregated. Space-time
aggregation means that one record in the attribute table represents
one point with all values measured in all specified years.

.. code-block:: python

   request.st_aggregated = True

**Download LUCAS points based on request**

Download data by using :meth:`~st_lucas.io.LucasIO.download` method using on the prepared request

.. code-block:: python
                
   lucasio.download(request)

or optionally by :meth:`~st_lucas.io.LucasIO.build` method as a helper
function to test the requrest without dowloading the data

.. code-block:: python
                
   request.build()

**Convert downloaded data**

Get data in specified format by
:meth:`~st_lucas.io.LucasIO.to_geopandas` method to convert the data
into GeoDataFrame object

.. code-block:: python

   lucasio.to_geopandas()

by :meth:`~st_lucas.io.LucasIO.to_gml` method to convert data into OGC
GML format

.. code-block:: python

   lucasio.to_gml()

by :meth:`~st_lucas.io.LucasIO.to_gpkg()` method to save the data in a
OGC GeoPackage file

.. code-block:: python
                
   lucasio.to_gpkg('sample.gpkg')

.. toctree::
   :glob:
   :numbered:

   notebooks/*

